(function() {
    'use strict';

    angular
        .module('www')
        .directive('menu', menuDirective);

    /** @ngInject */
    function menuDirective() {
        var directive = {
            restrict: 'A',
            scope: {
                'menu': '='
            },
            templateUrl: 'app/menu/menu.html',
            controller: MenuController,
            controllerAs: 'vm',
            bindToController: true,
        };

        return directive;

        /** @ngInject */
        function MenuController($location, $mdSidenav, menuStorage) {
            var vm = this;
            vm.menu = this;
            vm.menuItems = menuStorage.menuItems;
            vm.toggleMenu = function() {
                $mdSidenav('menu')
                    .toggle()
                    .then(function() {});
            }
            vm.closeMenu = function() {
                $mdSidenav('menu')
                    .close();
            }
            vm.isActive = function(location) {
                return location === $location.path();
            }
        }

    }
})();
