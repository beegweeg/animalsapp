(function() {
    'use strict';

    angular
        .module('www')
        .controller('ContactController', ContactController)
        .directive('validateEmail', validateEmailDirective);

    /** @ngInject */
    function ContactController($scope, $log) {
        var vm = this;
        vm.minMessageLength = 10;
        $scope.submit = function() {
            if (this.contactForm.$valid) {
                $log.log('email: ' + this.email);
                $log.log('message: ' + this.message);
            }
        }
    }

    function validateEmailDirective() {
        var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;

        return {
            require: 'ngModel',
            restrict: '',
            link: function(scope, elm, attrs, ctrl) {
                // only apply the validator if ngModel is present and Angular has added the email validator
                if (ctrl) {

                    // this will overwrite the default Angular email validator
                    ctrl.$validators.email = function(modelValue) {
                        return ctrl.$isEmpty(modelValue) || EMAIL_REGEXP.test(modelValue);
                    };
                }
            }
        }
    }
})();
