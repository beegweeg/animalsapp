(function() {
    'use strict';

    angular
        .module('www')
        .service('menuStorage', function() {
            this.menuItems = [{
                url: '/cats',
                templateUrl: 'app/cats/cats.html',
                title: 'Cats',
            }, {
                url: '/dogs',
                templateUrl: 'app/dogs/dogs.html',
                title: 'Dogs',
            }, {
                url: '/horses',
                templateUrl: 'app/horses/horses.html',
                title: 'Horses',
            }, {
                url: '/contact',
                templateUrl: 'app/contact/contact.html',
                title: 'Contact us',
                controller: 'ContactController',
                controllerAs: 'contact'
            }, ]
        })
        .config(routeConfig);

    function routeConfig($routeProvider, menuStorageProvider) {
        var menuStorageService = menuStorageProvider.$get();
        $routeProvider.when('/', { });
        angular.forEach(menuStorageService.menuItems, function(r) {
            $routeProvider.when(r.url, { templateUrl: r.templateUrl, controller: r.controller });
        });
        $routeProvider.when('otherwise', { templateUrl: 'menuStorageService[0].templateUrl' });
    }

})();
