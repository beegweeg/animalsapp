/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('www')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
