(function() {
    'use strict';

    angular
        .module('www')
        .directive('main', mainDirective);

    /** @ngInject */
    function mainDirective() {
        var directive = {
            restrict: 'E',
            templateUrl: 'app/main/main.html',
            controller: MainController,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        /** @ngInject */
        function MainController($location, $mdDialog, menuStorage) {
            var vm = this;
            vm.$mdDialog = $mdDialog;
            vm.menuItems = menuStorage.menuItems;
            vm.showConfirm = function(ev) {
                var confirm = $mdDialog.confirm()
                    .title('help modal')
                    .textContent('')
                    .ariaLabel('')
                    .targetEvent(ev)
                    .ok('Yes')
                    .cancel('No');
                $mdDialog.show(confirm).then(function() {
                    console.log('yes');
                }, function() {
                    console.log('no');
                });
            };
            vm.toggleMenu = function() {
              vm.menu.toggleMenu();
            }
            vm.isActive = function(location) {
                return location === $location.path();
            }
        }
    }
})();