(function() {
  'use strict';

  angular
    .module('www', ['ngAnimate', 'ngResource', 'ngRoute', 'ui.bootstrap', 'toastr', 'ngMaterial']);

})();
